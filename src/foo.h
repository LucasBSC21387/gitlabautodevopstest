#ifndef FOO_H
#define FOO_H

#pragma once

class foo
{
    // Data
    private:
    public:
    // Methods
    private:
    public:
        void vecAdd(float *a, float *b, float *c, int n);
        float dotProduct(float *a, float *b, int n);
        float vecSum(float *a, int n);
};

#endif