#include <iostream>
#include <string>
#include "foo.h"

int main(void)
{
    /// Initialize error code to 0
    int errcode = 0;

    // Set up arrays size
    const unsigned int narr = 100;

    // Create 3 float arrays
    float *a = new float[narr];
    float *b = new float[narr];
    float *c = new float[narr];

    // Initialize the arrays
    for (unsigned int i = 0; i < narr; i++)
    {
        a[i] = (float) i;
        b[i] = 1.0f;
        c[i] = 0.0f;
    }

    // Create a foo object
    foo f;

    // try the vecAdd method, return error code -10 if it fails
    try
    {
        // Run the method vecAdd
        f.vecAdd(a, b, c, narr);
        // Set a random integer between 0 and narr-1
        unsigned int i = rand() % narr;
        // Check the result
        if (c[i] != a[i] + b[i])
        {
            std::cout << "Error 10: c[" << i << "] = " << c[i] << " != " << a[i] << " + " << b[i] << std::endl;
            int err = -10;
            throw (err);
        }
    }
    catch(int c)
    {
        errcode = c;
        return errcode;
    }

    return 0;
}